/**
 * Solution to Problem 1
 */
public class H1P1 {
    public static void main(String[] args) {
        bmiPrinter(bmiOne(50, 1.6)); // the BMI value is 19.53
        bmiPrinter(bmiOne(72, 1.82));
        bmiPrinter(bmiOne(50, 1.65));
        bmiPrinter(bmiOne(90, 1.80));
        bmiPrinter(bmiOne(55, 1.77));
        bmiPrinter(bmiTwo(155, 72)); // // the BMI value is 21.02
        bmiPrinter(bmiTwo(220, 66));
        bmiPrinter(bmiTwo(150, 74));
        bmiPrinter(bmiTwo(153, 58));
        bmiPrinter(bmiTwo(200, 62));
    }

    public static double bmiOne(double m, double h) { // Calculates BMI from metric
        return m/(h * h);
    }

    public static double bmiTwo(double w, double h) { // Calculates BMI from imperial
        return w/(h * h) * 703;
    }

    public static void bmiPrinter(double bmi) { // print table
        System.out.printf("Your BMI value is: %.2f." +
                " Please refer to the table bellow to the " +
                "check the category for this value:\n",
                bmi);
        System.out.println("----------------------------------------");
        System.out.println("|       BMI         |   Category       |");
        System.out.println("----------------------------------------");
        System.out.println("| From 16.0 to 18.5 | Underweight      |");
        System.out.println("----------------------------------------");
        System.out.println("| From 18.0 to 25   | Normal           |");
        System.out.println("----------------------------------------");
        System.out.println("| From 25 to 30     | Overweight       |");
        System.out.println("----------------------------------------");
        System.out.println("| From 30 to 35     | Moderately obese |");
        System.out.println("----------------------------------------");
        System.out.println("| From 35 to 40     | Severely obese   |");
        System.out.println("----------------------------------------");
    }
}
