import java.util.Scanner;

/**
 * Solution to Problem 2
 *
 */
public class H1P2 {
    public static void main(String[] args) {
        // Half 1
        System.out.println("Please enter your amount in cents"); // cents to dollars
        Scanner keyboardInput = new Scanner(System.in);
        int centsInput = keyboardInput.nextInt();
        String dollarsOutput = change(centsInput);
        System.out.println(dollarsOutput);

        // Half 2
        Scanner keyboardInput2 = new Scanner(System.in);
        System.out.println("Please enter your amount in dollars");
        int dollarsInput = keyboardInput2.nextInt();
        System.out.println("Enter your quarter amount");
        int quarters = keyboardInput2.nextInt();
        System.out.println("Enter your dime amount");
        int dimes = keyboardInput2.nextInt();
        System.out.println("Enter your nickel amount");
        int nickels = keyboardInput2.nextInt();
        System.out.println("Enter your cent amount");
        int cents = keyboardInput2.nextInt();

        String centsOutput =
            inCents(
                dollarsInput,
                quarters,
                dimes,
                nickels,
                cents);

        System.out.println(centsOutput);
    }

    public static String change(int inputCents) {
        int dollars = inputCents / 100;
        int dollarsRemainder = inputCents % 100;

        int quarters = dollarsRemainder / 25;
        int quartersRemainder = dollarsRemainder % 25;

        int dimes = quartersRemainder / 10;
        int dimesRemainder = quartersRemainder % 10;

        int nickels = dimesRemainder / 5;
        int cents = dimesRemainder % 5;

        return inputCents + " cents corresponds to: \n" +
                "\t" + dollars + " dollars, " +
                quarters + " quarters, " +
                dimes + " dimes, \n" +
                "\t" + nickels + " nickels, " +
                cents + " and cents. \nwhere " +
                dollars + " is the computed number of dollars and so on.";
    }

    public static String inCents(
            int dollars,
            int quarters,
            int dimes,
            int nickels,
            int cents)
    {
        int totalCents = dollars * 100 +
                quarters * 25 +
                dimes * 10 +
                nickels * 5 +
                cents;
        return dollars + " dollars, " +
                quarters + " quarters, " +
                dimes + " dimes, " +
                nickels + " nickels, " +
                cents + " cents correspond to:\n" + totalCents + " cents.";
    }
}