public class Messy {
    /**
     * I made changes to constant variable by making it uppercase.
     * The variable name needs to be in lowercase because it is correct Java convention.
     * I added spaces because the assignment operator and string concatenation.
     * Moved "public static final double DISTANCE = 6.21;" to the top.
     * Changed the method name to lowercase.
     */

    public static final double DISTANCE = 6.21;

    public static double moneyOwed(double x) {
        return x * 1.6;
    }

    public static void main(String[] args) {
        double time;
        double pace;
        System.out.println("This program calculates your pace given a time and distance traveled.");
        time = 35.5; /* 35 minutes and 30 seconds */
        pace = time / DISTANCE;
        System.out.println("Your pace is " + pace + " miles per hour.");
    }
}