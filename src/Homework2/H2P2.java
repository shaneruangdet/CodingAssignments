import java.util.Scanner;

public class H2P2 {
  public static void main(String[] args) {
    Scanner kb = new Scanner(System.in);
    String input = kb.nextLine();
    Scanner keyboard = new Scanner(input).useDelimiter("\\s*,\\s*"); // Must use delimiter to accept ", " in Scanner
    String name = keyboard.next();
    int basePay = keyboard.nextInt();
    int hoursWorked = keyboard.nextInt();
    int returnValue;
    returnValue = salaryTotal(basePay, hoursWorked);
    if(returnValue == -1) {
      System.out.println("The base salary you entered does not comply with state law.");
    }
    else if(returnValue == -2) {
      System.out.println("The number of hours you entered does not comply with company policy");
    }
    else {
      System.out.println("The total pay for " + name + " is " + returnValue + "dollars"); // total paid
    }

  }

  public static int salaryTotal(int basePay, int hoursWorked)
  {
    if(basePay < 5.25) { // Not comply with state law
      return -1;
    }

    else if(hoursWorked > 60) { // Not comply with company policy
      return -2;
    }

    else {
      int totalPay = basePay * hoursWorked;
      return totalPay;
    }

  }
}
