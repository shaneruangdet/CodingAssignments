import java.util.Scanner;

public class H2P1 {
    public static void main(String[] args) {
        System.out.println("Enter the a, b, and c of your quadratic function (separated by spaces:) "); // user input
        Scanner keyboard = new Scanner(System.in);
        int a = keyboard.nextInt();
        int b = keyboard.nextInt();
        int c = keyboard.nextInt();
        int x = (-b / (2 * a)); // quadratic formula
        int y = a * (x * x) + (b * x) + c;
        System.out.println("The vertex of the corresponding parabola is: (" + x + ", " + y + ")");
        if(y == 0 && x == 0) {
            System.out.println("This vertex is also the zero of your parabola");
        }
        if(x == 0) {
            System.out.println("This vertex is also the y-intercept of you parabola");
        }
    }
}
